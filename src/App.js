import React from 'react'
import Game from './components/Game'

function App() {
  return (
    <div className="App">
      <h2 style={{ textAlign: 'center' }}>Tic Tac Toe Game</h2>
      <Game />
    </div>
  );
}

export default App;
